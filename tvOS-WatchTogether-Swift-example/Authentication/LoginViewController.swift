//
//  LoginViewController.swift
//  Example_tvos
//
//  Created by Igor Nazarov on 27.01.2021.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var roomNameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    var token = "yourRoomToken"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Actions
    
    @IBAction func onLogin(_ sender: Any) {
        guard let room = roomNameTextField.text,
              !room.isEmpty else {
            print("Please provide room name")
            return
        }

        getToken(for: room)
    }
    
    // MARK: - Authentication
    
    private func getToken(for room: String) {
        //MARK: Please check and put your token
        assert(token != "yourRoomToken")
        self.showSession(with: token)
    }
    
    // MARK: - Navigation
    
    private func showSession(with token: String) {
        DispatchQueue.main.async {
            guard let storyboard = self.storyboard,
                  let vc = storyboard.instantiateViewController(withIdentifier: "SessionViewController") as? SessionViewController else {return}
            
            vc.token = token
            vc.username = self.usernameTextField.text ?? UUID().uuidString
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

