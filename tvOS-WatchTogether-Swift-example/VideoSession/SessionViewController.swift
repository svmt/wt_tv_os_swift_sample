//
//  SessionViewController.swift
//  Example_tvos
//
//  Created by Igor Nazarov on 27.01.2021.
//

import UIKit
import WatchTogether_tvos
import SynchSDK_tvos
import AVKit

class SessionViewController: UIViewController {
    
    @IBOutlet weak var participantsCollectionView: UICollectionView!
    @IBOutlet weak var playerHolderView: UIView!
    
    @IBOutlet weak var clientsLabel: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var deltaLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var receivedMessageLabel: UILabel!
    
    var player: AVPlayer!
    
    var syncSDK: SynchSDK?

    var token: String!
    var username: String!
    
    private var room = "yourRoom"
    private var accessToken = "yourAccessToken"
    
    var session: Session?
    var localParticipant: LocalParticipant?
    var participants = [Participant]()

    // MARK: View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK: please set this variables to your before start
        assert(room != "yourRoom")
        assert(accessToken != "yourAccessToken")
        participantsCollectionView.delegate = self
        participantsCollectionView.dataSource = self
        startSession()
        setUpSynch()
//        setUpStream(type: .football) // LIVE STREAM
        setUpStream(type: .bipbop) // VOD
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        session?.disconnect()
        session = nil
    }
    
    func setUpStream(type: VideoTypes) {
        setPlayer(mediaURL: type.getVideoURL())
    }
    
    func setPlayer(mediaURL: URL) {
        player = AVPlayer(url: mediaURL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.playerHolderView.bounds
        self.playerHolderView.layer.addSublayer(playerLayer)
        self.playerHolderView.layoutSubviews()
    }
    
    // MARK: Synch
    func setUpSynch() {
        syncSDK = SynchSDK(accessToken: accessToken)
        syncSDK?.attachListener(self)
    }
    
    func createSyncRoom(with id: String, clientId: String){
        syncSDK?.createGroup(id, clientName: clientId)
    }
    
    func startSync() {
        syncSDK?.createGroup(room, clientName: "tvOS_client\(Int.random(in: 0...4))")
        syncSDK?.startSynchronize()
    }
    
    func stopSync() {
        syncSDK?.stopSynchronize()
    }
    
    // MARK: WatchTogether
    private func startSession() {
        startVideoSession()
    }
    
    private func startVideoSession() {
        Session.setMinLogLevel( .debug)
        session = SessionBuilder()
            .withUsername(username)
            .withVideoCodec(.H264)
            .withVideoRenderer(.Metal)
            .withDelegate(self)
            .build()
        localParticipant = session?.localParticipant
        session?.connect(with: token)
    }
    
    private func removeParticipant(_ participant: Participant) {
        participants.removeAll { (element) -> Bool in
            element.getId() == participant.getId()
        }
        
        participantsCollectionView.reloadData()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        guard let text = messageTextField.text else { return }
        session?.sendMessage(message: text)
    }
    
    
    //MARK: Player actions
    @IBAction func playAction(_ sender: UIButton) {
        player.play()
        syncSDK?.playerPlay()
    }
    @IBAction func pauseAction(_ sender: UIButton) {
        player.pause()
        syncSDK?.playerPause()
    }
    
    @IBAction func startSynch(_ sender: UIButton) {
        startSync()
    }
    @IBAction func stopSynch(_ sender: UIButton) {
        stopSync()
    }
    
    @IBAction func seekBack(_ sender: UIButton) {
        let time = player.currentTime()
        let newTime = CMTime(seconds: time.seconds - 30, preferredTimescale: 1)
        player.seek(to: newTime)
        syncSDK?.playerSeek(position: Int(newTime.seconds * 1000))
    }
    @IBAction func seekForward(_ sender: UIButton) {
        let time = player.currentTime()
        let newTime = CMTime(seconds: time.seconds + 30, preferredTimescale: 1)
        player.seek(to: newTime)
        syncSDK?.playerSeek(position: Int(newTime.seconds * 1000))
    }
}

extension SessionViewController: SessionDelegate {
    func onRemoteParticipantNotification(message: String, participantId: String) {
        if let participant = participants.filter{ $0.getId() == participantId }.first {
            DispatchQueue.main.async {
                self.receivedMessageLabel.text = "\(participant.getDisplayName()): \(message)"
            }
           
        }
        
    }
    
    func onRemoteParticipantStopMedia(participant: Participant) {
        removeParticipant(participant)
    }
    
    
    func onSessionError(error: Error) {
        print(error.localizedDescription)
    }
    
    func onSessionConnected(sessionId: String, participants: [Participant]) {
        let broadcasters = participants.filter { $0.isBroadcaster }
        self.participants.append(contentsOf: broadcasters)
        participantsCollectionView.reloadData()
    }
    
    func onSessionDisconnect() {
        
    }
    
    func onRemoteParticipantStartMedia(participant: Participant) {
        participants.append(participant)
        participantsCollectionView.reloadData()
    }
    
    func onRemoteParticipantLeft(participant: Participant) {
        removeParticipant(participant)
    }
}

extension SessionViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return participants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParticipantCollectionViewCell", for: indexPath) as? ParticipantCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.participant = participants[indexPath.row]
        return cell
    }
 
}

extension SessionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 200)
    }
}
extension SessionViewController: SynchListener {
    func onClientList(clientList: [Client]) {
        clientsLabel.text = ""
        for client in clientList {
            clientsLabel.text! += "\(client.name) \n"
        }
    }
    
    func onSetPlaybackRate(rate: Float) {
        if player.rate != 0 {
            player.rate = rate
        }
    }
    
    func onPlaybackFromPosition(position: Int, participantId: String) {
        player?.seek(to: CMTime(seconds: Double(position)/1000, preferredTimescale: 1))
    }
    
    func onPause(participantId: String) {
        player.pause()
    }
    
    func onResumePlay(participantId: String) {
        player.play()
    }
    
    func onGetPlayerPosition() -> Int {
        //for Live Stream: currentDate()
        if let currentDate = player?.currentItem?.currentDate() {
            let timeInterval = currentDate.timeIntervalSince1970
            return Int(timeInterval * 1000)
        } else {//for VOD: currentTime()
            let currentTime = player?.currentTime()
            let timeInterval = currentTime?.seconds
            return Int((timeInterval ?? 0) * 1000)
        }
    }
    
    func onGetPlaybackRate() -> Float {
        return player.rate
    }
    
    func onSyncInfo(accuracy: Float, delta: Int) {
        DispatchQueue.main.async {
            self.accuracyLabel.text = "\(accuracy)"
            self.deltaLabel.text = "\(delta)"
        }
    }
    
    
}

enum VideoTypes {
    case football
    case bipbop
    
    func getVideoURL() -> URL {
        switch self {
        case .football:
            return URL(string: "https://demo-app.sceenic.co/football.m3u8")!
        case .bipbop:
            return URL(string: "https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8")!
        }
    }
}
