//
//  ParticipantCollectionViewCell.swift
//  Example_tvos
//
//  Created by Igor Nazarov on 18.02.2021.
//

import UIKit
import WatchTogether_tvos

class ParticipantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    var participant: Participant! {
        didSet {
            let view = participant.getVideo()
            view.frame = bounds
            containerView.addSubview(view)
        }
    }
}
