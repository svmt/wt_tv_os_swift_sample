//
//  VideoRoomLayout.swift
//  Example_tvos
//
//  Created by Igor Nazarov on 19.02.2021.
//

import UIKit

class VideoRoomLayout: UICollectionViewLayout {
    
    var participantsCount: Int {
        guard let collectionView = self.collectionView else {
            return 0
        }
        
        return collectionView.numberOfItems(inSection: 0)
    }
    
    lazy var grid = Grid(numberOfItems: participantsCount)
    
    private var cache = [UICollectionViewLayoutAttributes]()
    
    override var collectionViewContentSize: CGSize {
        guard let collectionView = self.collectionView else {
            return .zero
        }
        
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else {
            return
        }
        
        cache.removeAll()
        
        grid = Grid(numberOfItems: participantsCount)
        
        let columnWidth = collectionViewContentSize.width / CGFloat(grid.columns)
        let rowHeight = collectionViewContentSize.height / CGFloat(grid.rows)
        
        for item in 0..<collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            let row = grid.row(for: item)
            let column = grid.column(for: item)
            
            let xOffset = CGFloat(column) * columnWidth
            let yOffset = CGFloat(row) * rowHeight
            let frame = CGRect(x: xOffset,
                               y: yOffset,
                               width: columnWidth,
                               height: rowHeight)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = frame
            cache.append(attributes)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
}
