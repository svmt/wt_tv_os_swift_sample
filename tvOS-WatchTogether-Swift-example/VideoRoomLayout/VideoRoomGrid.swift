//
//  VideoRoomGrid.swift
//  Example_tvos
//
//  Created by Igor Nazarov on 22.02.2021.
//

import Foundation

extension VideoRoomLayout {
    
    struct Grid {
        let rows: Int
        let columns: Int
        
        init(numberOfItems: Int) {
            guard numberOfItems > 0 else {
                rows = 1
                columns = 1
                return
            }
            
            rows = Int(sqrt(Double(numberOfItems)))
            
            guard rows > 1 else {
                columns = numberOfItems
                return
            }
            
            columns = numberOfItems % rows == 0 ? numberOfItems / rows : numberOfItems / rows + 1
        }
        
        func row(for item: Int) -> Int {
            if item < columns {
                return 0
            }
            
            return item / columns
        }
        
        func column(for item: Int) -> Int {
            return item % columns
        }
    }
}
